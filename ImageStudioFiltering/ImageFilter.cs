﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Configuration;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Studio.Processing.Helper;
using Studio.Processing.LogicController;
using Studio.Processing.StudioEnum;
using Studio.Processing.ViewModel;

namespace ImageStudioFiltering
{
    public static class ImageFilter
    {
        public static Graphics imageGraphicsOut;
        public static Bitmap bm;

        public static Image ImageFiltered(this Image inputImage, Color colorValue, StudioProcessingImage processingImage)
        {
            string processType = Enum.GetName(typeof(StudioProcessingImage), processingImage);
            bm = new Bitmap(inputImage);
            bm = BitmapController.bmProcessed(processType, bm, 0, inputImage);
            ProcessingImage imgProcessing = new ProcessingImage();
            imgProcessing = BitmapController.ProcessingImage;
            imageGraphicsOut = imgProcessing.Graphics;

            //Filtering Image, for example blue, color from Argb(alpha, red, green, blue)
            imageGraphicsOut.FillRectangle(new SolidBrush(Color.FromArgb(100, colorValue)), 0, 0, bm.Width, bm.Height);

            return bm;
        }

        public static Image ImageRotate(this Image processImage, StudioProcessingImage processingImage)
        {
            bm = new Bitmap(processImage);
            string processType = Enum.GetName(typeof(StudioProcessingImage), processingImage);
            bm = BitmapController.bmProcessed(processType, bm, 0, processImage);

            return bm;
        }

        public static Image AdjustFilterRGBHSL(this Image img, Bitmap adjBitmap, int value,
            StudioProcessingImage processingImage)
        {
            string processingType = Enum.GetName(typeof(StudioProcessingImage), processingImage);
            bm = BitmapController.bmProcessed(processingType, adjBitmap, value, img);

            return bm;
        }

        public static Image ZoomImage(Image img, Size size)
        {
            Bitmap bm = new Bitmap(img, img.Width + (img.Width * size.Width / 100), img.Height + (img.Height * size.Height / 100));
            Graphics grfx = Graphics.FromImage(bm);
            grfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grfx.Dispose();

            return bm;
        }
    }
}
