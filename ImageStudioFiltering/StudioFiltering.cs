﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Studio.Processing.Helper;
using Studio.Processing.StudioEnum;
using MetroFramework.Forms;

namespace ImageStudioFiltering
{
    public partial class StudioFiltering : MetroForm
    {
        Graphics graphics;
        Image buffer;
        Bitmap newBitmap;
        Color setColor;
        Bitmap newBitmapOnChange;
       
        private string _processValue = string.Empty;
        private StudioProcessingImage NewprocessingImage;
        private string ContrastValue = "0";
        private string BrightnessValue = "0";
        private string SaturationValue = "0";
        private string HueValue = "0";
        private string LuminanceValue = "0";
        private string prevCheckedItem = String.Empty;
        private int OldValue = 0;

        public StudioFiltering()
        {
            InitializeComponent();
            this.comboBox1.Items.Add("--- None Filter ---");
            foreach (var clr in Enum.GetValues(typeof(KnownColor)))
            {
                Color color = Color.FromKnownColor((KnownColor)clr);
                this.comboBox1.Items.Add(color.Name);
            }
            this.comboBox1.SelectedIndex = 0;

            foreach (var stdFilterType in Enum.GetNames(typeof(StudioFilteringEnum)))
            {
                if (stdFilterType != null && stdFilterType != String.Empty)
                {
                    this.checkedListBox1.Items.Add(stdFilterType);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (this.pictureBox1.Image == null)
            {
                this.metroTrackBar1.Enabled = false;
                this.metroTrackBar1.Visible = false;
                this.comboBox1.Enabled = false;
                this.button1.Enabled = false;
                this.button2.Enabled = false;
                this.button3.Enabled = false;
                this.metroLabel1.Visible = false;
                this.FilterLevelLabel.Visible = false;
                this.button4.Enabled = false;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedItem.ToString() == "--- None Filter ---")
            {
                this.checkBox1.Checked = false;
                this.checkBox1.Enabled = false;
            }
            else
            {
                this.checkBox1.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.comboBox1.SelectedItem != null)
            {
                StudioProcessingImage processType = StudioProcessingImage.IsFilterColor;
                graphics = ImageFilter.imageGraphicsOut;
                if (!this.checkBox1.Checked)
                {
                    if (this.graphics != null)
                    {
                        Point[] points = { new Point(0, 0), new Point(pictureBox1.Image.Width, 0), new Point(0, pictureBox1.Image.Height) };
                        this.graphics.Clear(Color.Empty);
                        this.graphics.DrawImage(this.buffer, points);
                    }
                    pictureBox1.Refresh();
                }                

                if (this.comboBox1.SelectedItem.ToString() != "--- None Filter ---")
                {
                    string getColor = this.comboBox1.SelectedItem.ToString();
                    Color colorSet = Color.FromName(getColor);
                    pictureBox1.Image = pictureBox1.Image.ImageFiltered(colorSet, processType);
                    setColor = colorSet;
                }
                else
                {
                    Graphics grfx2 = Graphics.FromImage(pictureBox1.Image);
                    Color colorset = grfx2.GetNearestColor(setColor);
                    setColor = colorset;
                }
                newBitmap = new Bitmap(pictureBox1.Image);
            }
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "JPG(*.JPG)|*.jpg";

            if (f.ShowDialog() == DialogResult.OK)
            {
                buffer = Image.FromFile(f.FileName);
                this.pictureBox1.Image = buffer;

                Graphics grfx2 = Graphics.FromImage(this.pictureBox1.Image);
                Color colorset = grfx2.GetNearestColor(setColor);
                setColor = colorset;

                newBitmap = new Bitmap(pictureBox1.Image);
                metroTrackBar1.Value = Int32.Parse(Math.Round(setColor.GetBrightness()).ToString());
                FilterLevelLabel.Text = metroTrackBar1.Value.ToString();
            }

            if (this.pictureBox1.Image != null)
            {
                this.metroTrackBar1.Enabled = true;
                this.comboBox1.Enabled = true;
                this.button1.Enabled = true;
                this.button2.Enabled = true;
                this.button3.Enabled = true;
                this.button4.Enabled = true;
            }
        }

        private void btnSaveFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            f.Filter = "JPG(*.JPG)|*.jpg";
            f.FileName = "ThompsAchiStudio.jpg";
            if (f.ShowDialog() == DialogResult.OK)
            {
                this.pictureBox1.Image.Save(f.FileName);
            }
        }

        private void btnRotate_Click(object sender, EventArgs e)
        {
            StudioProcessingImage processType = StudioProcessingImage.IsRotate;
            this.pictureBox1.Image = pictureBox1.Image.ImageRotate(processType);
            newBitmap = new Bitmap(this.pictureBox1.Image);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            string prcsType = String.Empty;
            if (_processValue.Contains("Contrast"))
                prcsType = "IsContrast";
            else if (_processValue.Contains("Brightness"))
                prcsType = "IsBrightness";
            else if (_processValue.Contains("Saturation"))
                prcsType = "IsSaturation";
            else if (_processValue.Contains("Hue"))
                prcsType = "IsHue";
            else if (_processValue.Contains("Luminance"))
                prcsType = "IsLuminance";

            StudioProcessingImage processType = EnumHelper<StudioProcessingImage>.GetValueFromName(prcsType);
            this.pictureBox1.Image = pictureBox1.Image.AdjustFilterRGBHSL(newBitmapOnChange, this.metroTrackBar1.Value, processType);

            switch (processType)
            {
                case StudioProcessingImage.IsContrast:
                    ContrastValue = this.metroTrackBar1.Value.ToString();
                    break;
                case StudioProcessingImage.IsBrightness:
                    BrightnessValue = this.metroTrackBar1.Value.ToString();
                    break;
                case StudioProcessingImage.IsSaturation:
                    SaturationValue = this.metroTrackBar1.Value.ToString();
                    break;
                case StudioProcessingImage.IsHue:
                    HueValue = this.metroTrackBar1.Value.ToString();
                    break;
                case StudioProcessingImage.IsLuminance:
                    LuminanceValue = this.metroTrackBar1.Value.ToString();
                    break;
            }

            FilterLevelLabel.Text = this.metroTrackBar1.Value.ToString();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox chcklst = sender as CheckedListBox;
            if (chcklst == null)
            {
                throw new InvalidEnumArgumentException();
            }

            if (this.pictureBox1.Image != null)
            {
                if (e.NewValue == CheckState.Checked)
                {
                    this.metroTrackBar1.Enabled = true;
                    for (int i = 0; i < checkedListBox1.Items.Count; i++)
                    {
                        if (e.Index != i)
                        {
                            this.checkedListBox1.SetItemChecked(i, false);
                        }
                        else
                        {
                            _processValue = this.checkedListBox1.SelectedItem.ToString();
                            prevCheckedItem = _processValue;
                        }
                    }

                    switch (_processValue)
                    {
                        case "Brightness":
                            BrightnessValue = FilteringLevelCalc(BrightnessValue);
                            break;
                        case "Contrast":
                            ContrastValue = FilteringLevelCalc(ContrastValue);
                            break;
                        case "Saturation":
                            SaturationValue = FilteringLevelCalc(SaturationValue);
                            break;
                        case "Hue":
                            HueValue = FilteringLevelCalc(HueValue);
                            break;
                        case "Luminance":
                            LuminanceValue = FilteringLevelCalc(LuminanceValue);
                            break;
                    }

                    FilterLevelLabel.Text = metroTrackBar1.Value.ToString();

                    metroLabel1.Text = _processValue;
                    metroLabel1.Visible = true;
                    FilterLevelLabel.Visible = true;
                    metroTrackBar1.Visible = true;
                    metroTrackBar1.Enabled = true;
                    if (this.pictureBox1.Image != null)
                        newBitmapOnChange = new Bitmap(this.pictureBox1.Image);
                }
                else
                {
                    if (!this.checkedListBox1.CheckedItems.Contains(prevCheckedItem))
                    {
                        FilterLevelLabel.Text = metroTrackBar1.Value.ToString();

                        metroLabel1.Text = _processValue;
                        metroLabel1.Visible = true;
                        FilterLevelLabel.Visible = true;
                        metroTrackBar1.Enabled = true;
                    }
                    else
                    {
                        this.metroTrackBar1.Enabled = false;
                        this.metroTrackBar1.Visible = false;
                        this.metroLabel1.Visible = false;
                        FilterLevelLabel.Visible = false;
                    }
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (newBitmapOnChange != null)
            {
                newBitmapOnChange = newBitmap;
                this.pictureBox1.Image = buffer;
            }

            BrightnessValue = "0";
            ContrastValue = "0";
            SaturationValue = "0";
            HueValue = "0";
            LuminanceValue = "0";
            this.metroTrackBar1.Value = 0;
            this.FilterLevelLabel.Text = "0";
            this.comboBox1.SelectedItem = this.comboBox1.Items[0];
        }

        private string FilteringLevelCalc(string FilterLevel)
        {
            double a = Convert.ToDouble(FilterLevel);
            a = Math.Round(a);
            FilterLevel = a.ToString();
            metroTrackBar1.Value = Int32.Parse(FilterLevel);

            return FilterLevel;                        
        }

        private void metroScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            int threshold = 0;
            threshold = metroScrollBar1.Value;
            //threshold = e.NewValue < OldValue  ? (metroScrollBar1.Value - OldValue) : metroScrollBar1.Value;
            //OldValue = e.NewValue < OldValue ? Math.Abs(OldValue - metroScrollBar1.Value) : Math.Abs(threshold);
            this.pictureBox1.Image = ImageFilter.ZoomImage(buffer, new Size(threshold, threshold));
            newBitmap = new Bitmap(this.pictureBox1.Image);
        }
    }
}
