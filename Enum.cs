﻿using System;

public class StudioEnum
{
    enum FilteringType
    {
        Contrast = 0,
        Saturation = 1,
        Brightness = 2,
    }
}
