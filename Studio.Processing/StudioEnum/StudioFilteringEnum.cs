﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studio.Processing.StudioEnum
{
    public enum StudioFilteringEnum
    {
        Contrast = 0,       
        Brightness = 1,
        Saturation = 2,
        Hue = 3,
        Luminance = 4
    }

    public enum StudioProcessingImage
    {
        [Display (Name = "IsContrast")]
        IsContrast = 0,

        [Display (Name = "IsSaturation")]
        IsSaturation = 1,

        [Display (Name = "IsBrightness")]
        IsBrightness = 2,

        [Display (Name = "IsFilterColor")]
        IsFilterColor = 3,

        [Display(Name = "IsRotate")]
        IsRotate = 4,

        [Display(Name = "IsHue")]
        IsHue = 5,

        [Display(Name = "IsLuminance")]
        IsLuminance = 6
    }
}
