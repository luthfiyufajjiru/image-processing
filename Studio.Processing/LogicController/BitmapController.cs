﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Studio.Processing.Helper;
using Studio.Processing.StudioEnum;
using Studio.Processing.ViewModel;

namespace Studio.Processing.LogicController
{
    public static class BitmapController
    {
        public static Bitmap bm;
        public static ProcessingImage ProcessingImage;

        public static Bitmap bmProcessed(string processType, Bitmap adjBM, int byPassValue, Image img)
        {
            int processingValue = (int)EnumHelper<StudioProcessingImage>.GetValueFromName(processType);
            int _originPrcsValue = 0;
            float matrixValue = 0f;
            float[][] FloatMatrix = new float[5][];
            Graphics grfx = null;
            Bitmap newBM = null;
            Point[] points = new Point[3];
            if (processingValue == 2)
            {
                bm = adjBM;
                matrixValue = processingValue == 0 ? 0.04f * byPassValue : (float)byPassValue / 255.0f;
                newBM = new Bitmap(bm.Width, bm.Height);
                grfx = Graphics.FromImage(newBM);
            }
            else if ( processingValue == 1 || processingValue == 5 || processingValue == 6)
            {
                _originPrcsValue = processingValue;
                processingValue = 0;
            }

            switch (processingValue)
            {
                case 0:
                    newBM = new Bitmap(adjBM);
                    newBM = (_originPrcsValue != 0) ? ModifyHSL(newBM, byPassValue, _originPrcsValue, img) : Contrast(newBM, byPassValue, img);
                    ProcessingImage = new ProcessingImage();
                    ProcessingImage.Bitmap = newBM;
                    ProcessingImage.Graphics = Graphics.FromImage(newBM);
                    break;
                case 2:
                    FloatMatrix[0] = new float[] { 1f, 0f, 0f, 0f, 0f };
                    FloatMatrix[1] = new float[] { 0f, 1f, 0f, 0f, 0f };
                    FloatMatrix[2] = new float[] { 0f, 0f, 1f, 0f, 0f };
                    FloatMatrix[3] = new float[] { 0f, 0f, 0f, 1f, 0f };
                    FloatMatrix[4] = new float[] { matrixValue, matrixValue, matrixValue, 1f, 1f };

                    ColorMatrix NewColorMatrix = new ColorMatrix(FloatMatrix);
                    ImageAttributes adjAttributes = new ImageAttributes();
                    adjAttributes.SetColorMatrix(NewColorMatrix);
                    grfx.DrawImage(bm, new Rectangle(0, 0, bm.Width, bm.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel, adjAttributes);

                    ProcessingImage = new ProcessingImage();
                    ProcessingImage.Bitmap = newBM;
                    ProcessingImage.Graphics = grfx;

                    adjAttributes.Dispose();
                    grfx.Dispose();
                    break;
                case 3:
                    newBM = new Bitmap(img);
                    Graphics imageGraphics = Graphics.FromImage(newBM);
                    imageGraphics.Clear(Color.Transparent);
                    points[0] = new Point(0, 0);
                    points[1] = new Point(newBM.Width, 0);
                    points[2] = new Point(0, newBM.Height);
                    imageGraphics.DrawImage(img, points);
                    newBM.SetResolution(imageGraphics.DpiX, imageGraphics.DpiY);

                    ProcessingImage = new ProcessingImage();
                    ProcessingImage.Graphics = imageGraphics;
                    ProcessingImage.Bitmap = newBM;
                    break;
                case 4:
                    newBM = new Bitmap(img.Width, img.Height);
                    points[0] = new Point(0, 0);
                    points[1] = new Point(img.Width, 0);
                    points[2] = new Point(0, img.Height);
                    using (grfx = Graphics.FromImage(newBM))
                    {
                        grfx.Clear(Color.White);
                        grfx.DrawImage(img, points);
                    }

                    newBM.RotateFlip(RotateFlipType.Rotate90FlipNone);

                    ProcessingImage = new ProcessingImage();
                    ProcessingImage.Bitmap = newBM;
                    ProcessingImage.Graphics = grfx;
                    break;
            }

            return newBM;
        }

        public static Bitmap Contrast(this Bitmap sourceBitmap, int threshold, Image img)
        {
            BitmapData sourceData = sourceBitmap.LockBits(new Rectangle(0, 0,
                                        sourceBitmap.Width, sourceBitmap.Height),
                                        ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

            byte[] pixelBuffer = new byte[sourceData.Stride * sourceData.Height];

            Marshal.Copy(sourceData.Scan0, pixelBuffer, 0, pixelBuffer.Length);

            sourceBitmap.UnlockBits(sourceData);

            double contrastLevel = Math.Pow((100.0 + threshold) / 100.0, 2);

            double blue = 0;
            double green = 0;
            double red = 0;

            for (int k = 0; k + 4 < pixelBuffer.Length; k += 4)
            {
                blue = ((((pixelBuffer[k] / 255.0) - 0.5) *
                           contrastLevel) + 0.5) * 255.0;

                green = ((((pixelBuffer[k + 1] / 255.0) - 0.5) *
                            contrastLevel) + 0.5) * 255.0;

                red = ((((pixelBuffer[k + 2] / 255.0) - 0.5) *
                           contrastLevel) + 0.5) * 255.0;

                if (blue > 255)
                { blue = 255; }
                else if (blue < 0)
                { blue = 0; }

                if (green > 255)
                { green = 255; }
                else if (green < 0)
                { green = 0; }

                if (red > 255)
                { red = 255; }
                else if (red < 0)
                { red = 0; }

                pixelBuffer[k] = (byte)blue;
                pixelBuffer[k + 1] = (byte)green;
                pixelBuffer[k + 2] = (byte)red;
            }

            Bitmap resultBitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);

            BitmapData resultData = resultBitmap.LockBits(new Rectangle(0, 0,
                                        resultBitmap.Width, resultBitmap.Height),
                                        ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);

            Marshal.Copy(pixelBuffer, 0, resultData.Scan0, pixelBuffer.Length);
            resultBitmap.UnlockBits(resultData);

            return resultBitmap;
        }

        public static Bitmap ModifyHSL(this Bitmap bmSource, int threshold, int processingValue, Image img)
        {
            double thValue = processingValue != 5 ? ((Convert.ToDouble(threshold)) / 100) : ((Convert.ToDouble(threshold)) / 100) * 360;
            Bitmap resultBitmap = new Bitmap(bmSource.Width, bmSource.Height);
            BitmapData data = bmSource.LockBits(new Rectangle(0, 0, bmSource.Width, bmSource.Height),
                ImageLockMode.ReadOnly, bmSource.PixelFormat);
            BitmapData resultData = resultBitmap.LockBits(new Rectangle(0, 0, resultBitmap.Width, resultBitmap.Height),
                ImageLockMode.WriteOnly, PixelFormat.Format32bppPArgb);

            unsafe
            {
                byte[] pixelBuffer = new byte[data.Stride * data.Height];
                Marshal.Copy(data.Scan0, pixelBuffer, 0, pixelBuffer.Length);
                for (int k = 0; k + 4 < pixelBuffer.Length; k += 4)
                {
                    ViewModelRGB processColor = new ViewModelRGB();
                    processColor = GetHSLAdjustment(pixelBuffer[k + 2], pixelBuffer[k + 1], pixelBuffer[k], thValue, processingValue);

                    pixelBuffer[k] = processColor.Blue;
                    pixelBuffer[k + 1] = processColor.Green;
                    pixelBuffer[k + 2] = processColor.Red;
                }
                Marshal.Copy(pixelBuffer, 0, resultData.Scan0, pixelBuffer.Length);
            }

            bmSource.UnlockBits(data);
            resultBitmap.UnlockBits(resultData);

            return resultBitmap;
        }

        public static ViewModelRGB GetHSLAdjustment(Byte R, Byte G, Byte B, double thValue, int processingValue)
        {
            HSL hsl = RGB_to_HSL(R, G, B);
            switch (processingValue)
            {
                case 1:
                    hsl.Saturation += thValue;
                    hsl.Saturation = hsl.Saturation > 1 ? 1 : (hsl.Saturation < -1 ? -1 : hsl.Saturation);
                    break;
                case 5:
                    hsl.Hue += thValue;
                    hsl.Hue = hsl.Hue > 360 ? 360 : (hsl.Hue < -360 ? -360 : hsl.Hue);
                    break;
                case 6:
                    hsl.Luminance += thValue;
                    hsl.Luminance = hsl.Luminance > 1 ? 1 : (hsl.Luminance < -1 ? -1 : hsl.Luminance);
                    break;
            }

            return HSL_to_RGB(hsl);
        }

        public static HSL RGB_to_HSL(Byte R, Byte G, Byte B)
        {
            float _R = (R / 255f);
            float _G = (G / 255f);
            float _B = (B / 255f);

            float _Min = Math.Min(Math.Min(_R, _G), _B);
            float _Max = Math.Max(Math.Max(_R, _G), _B);
            float _Delta = _Max - _Min;

            float H = 0, S = 0;
            float L = (float) ((_Max + _Min) / 2.0f);

            if (_Delta != 0f)
            {
                S = L < 0.5f ? (float)(_Delta / (_Max + _Min)) : (float)(_Delta / (2.0f - _Max - _Min));

                if (_R == _Max)
                    H = (((_G - _B) / _Delta) % 6) * 60;
                else if (_G == _Max)
                    H = (2f + ((_B - _R) / _Delta)) * 60;
                else if (_B == _Max)
                    H = (4f + ((_R - _G) / _Delta)) * 60;
            }

            HSL hslSet = new HSL();
            hslSet.Hue = H;
            hslSet.Saturation = S;
            hslSet.Luminance = L;

            return hslSet;
        }

        public static ViewModelRGB HSL_to_RGB(HSL hsl)
        {
            ViewModelRGB RGB = new ViewModelRGB();
            byte r, g, b;
            if (hsl.Saturation == 0)
            {
                r = (byte)Math.Round(hsl.Luminance * 255d);
                g = (byte)Math.Round(hsl.Luminance * 255d);
                b = (byte)Math.Round(hsl.Luminance * 255d);
            }
            else
            {
                double t1, t2;
                double th = hsl.Hue / 360.0d;
                t2 = hsl.Luminance < 0.5d ? hsl.Luminance * (1d + hsl.Saturation) : (hsl.Luminance + hsl.Saturation) - (hsl.Luminance * hsl.Saturation);
                t1 = (2d * hsl.Luminance) - t2;
                r = (byte)Math.Round(ColorCalc((th + (1.0d / 3.0d)), t1, t2) * 255d);
                g = (byte)Math.Round(ColorCalc(th, t1, t2) * 255d);
                b = (byte)Math.Round(ColorCalc((th - (1.0d / 3.0d)), t1, t2) * 255d);
            }

            RGB.Red = r;
            RGB.Green = g;
            RGB.Blue = b;

            return RGB;
        }

        private static double ColorCalc(double c, double t1, double t2)
        {

            if (c < 0) c += 1d;
            if (c > 1) c -= 1d;
            if (6.0d * c < 1.0d) return t1 + (t2 - t1) * 6.0d * c;
            if (2.0d * c < 1.0d) return t2;
            if (3.0d * c < 2.0d) return t1 + (t2 - t1) * (2.0d / 3.0d - c) * 6.0d;
            return t1;
        }
    }
}
