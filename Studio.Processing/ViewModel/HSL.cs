﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studio.Processing.ViewModel
{
    public class HSL
    {
        public double Hue { get; set; }
        public double Saturation { get; set; }
        public double Luminance { get; set; }
    }
}
